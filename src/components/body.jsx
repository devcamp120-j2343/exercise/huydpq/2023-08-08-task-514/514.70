import { Component } from "react";
import Content from "./Content/contentComponent";
import Header from "./Header/headerComponent";
import Footer from "./Footer/footerComponent";

class Body extends Component {
    render(){
        return (
            <>
            <Header/>
            <Content/>
            <Footer/>
            </>
        )
    }
}

export default Body
