import { Component } from "react";

class Footer extends Component {
    render(){
        return (
            <>
             {/* <!-- Footer --> */}
      <div className="container-fluid p-5 bg-warning mt-5">
        <div className="row text-center">
          <div className="col-sm-12">
            <h5 className="m-2"><b>Footer</b></h5>
            <a href="/#" className="btn btn-dark m-3"><i className="fa fa-arrow-up"></i> &nbsp;To the top</a>
            <div className="m-2">
              <i className="fa-brands fa-square-facebook"></i>
              <i className="fa-brands fa-instagram"></i>
              <i className="fa-brands fa-square-snapchat"></i>
              <i className="fa-brands fa-square-pinterest"></i>
              <i className="fa-brands fa-square-twitter"></i>
              <i className="fa fa-brands fa-linkedin w3-hover-opacity"></i>
            </div>
            <p className="m-2"><b>Powered by DEVCAMP</b></p>
          </div>
        </div>
      </div>
      {/* <!-- Vùng Modal --> */}
      {/* <!-- Modal tạo đơn hàng --> */}
      <div id="order-modal" className="modal fade" tabindex="-1">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Thông tin đơn hàng</h5>
              <button type="button" className="close" data-dismiss="modal">&times;</button>
            </div>
            {/* <!-- Modal body --> */}
            <div className="modal-body">
              <div className="form-group">
                <label>Họ và tên</label>
                <input type="text" className="form-control" id="modal-inp-fullname" />
              </div>
              <div className="form-group">
                <label>Số điện thoại</label>
                <input type="text" className="form-control" id="modal-inp-phone" />
              </div>
              <div className="form-group">
                <label>Địa chỉ</label>
                <input type="text" className="form-control" id="modal-inp-address" />
              </div>
              <div className="form-group">
                <label>Lời nhắn</label>
                <input type="text" className="form-control" id="modal-inp-message" />
              </div>
              <div className="form-group">
                <label>Mã giảm giá</label>
                <input type="text" className="form-control" id="modal-inp-voucherid" />
              </div>
              <div className="form-group">
                <label>Thông tin chi tiết</label>
                <div id="inp-info-detail" className="modal-box-scroll border"></div>
              </div>
            </div>
            <div className="modal-footer">
              <button id="btn-create-order-modal" className="btn btn-warning w-25 ">Tạo đơn</button>
              <button data-dismiss="modal" className="btn btn-danger w-25 ">Quay lại</button>
            </div>
          </div>
        </div>
      </div>
      {/* <!-- Modal kết quả gửi đơn --> */}
      <div id="result-order-modal" className="modal fade" tabindex="-1">
        <div className="modal-dialog modal-lg">
          <div className="modal-content bg-warning">
            <div className="modal-body">
              <button type="button" className="close" data-dismiss="modal"> &times;</button>
              <div className="form-group">
                <h5> <b>Cảm ơn bạn đã đặt hàng tại Pizza 365. Mã đơn hàng của bạn là:</b></h5>
              </div>
              <div className="row form-group">
                <label className="col-sm-3 col-form-label"><b>Mã đơn hàng</b></label>
                <div className="col-sm-9">
                  <input className="form-control" id="inp-modal-ordercode" />
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
            </>
        )
    }
}
export default Footer