import { Component } from "react";

class Content extends Component {
  render() {
    return (
      <>
        {/* <!-- Content --> */}
        <div className="container" style={{ paddingTop: "100px" }}>
          <div className="row">
            <div className="col-sm-12 text-warning">
              <h1><b>Pizza 365</b></h1>
              <i>Truly italian!</i>
            </div>

            {/* <!-- Slide row home --> */}
            <div className="col-sm-12 pt-2">
              <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                <ol className="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                </ol>
                <div className="carousel-inner">
                  <div className="carousel-item active">
                    <img className="d-block w-100" src={require("../../assets/images/1.jpg")} alt="First slide" height="600px" />
                  </div>
                  {/* <div className="carousel-item">
                            <img className="d-block w-100" src="images/2.jpg" alt="Second slide" height="600px"/>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src="images/3.jpg" alt="Third slide" height="600px"/>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src="images/4.jpg" alt="Forth slide" height="600px"/>
                        </div> */}
                </div>
                <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span className="carousel-control-next-icon" aria-hidden="true"></span>
                  <span className="sr-only">Next</span>
                </a>
              </div>
            </div>

            {/* <!-- Title what we offer --> */}
            <div className="col-sm-12 text-center text-warning p-4 mt-4">
              <h2><b className="p-2 border-bottom">Tại sao lại Pizza 365</b></h2>
            </div>

            {/* <!-- Content what we offer --> */}
            <div className="col-sm-12">
              <div className="row">
                <div className="col-sm-3 p-4 border-introduce" style={{ backgroundColor: "lightgoldenrodyellow" }} >
                  <h3 className="p-2">Đa dạng</h3>
                  <p className="p-2">Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay</p>
                </div>
                <div className="col-sm-3 p-4 border-introduce" style={{ backgroundColor: "yellow" }} >
                  <h3 className="p-2">Chất lượng</h3>
                  <p className="p-2">Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn
                    thực phẩm.
                  </p>
                </div>
                <div className="col-sm-3 p-4 border-introduce" style={{ backgroundColor: "lightsalmon" }} >
                  <h3 className="p-2">Hương vị</h3>
                  <p className="p-2">Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải ngiệm từ Pizza 365.</p>
                </div>
                <div className="col-sm-3 p-4 border-introduce bg-warning">
                  <h3 className="p-2">Dịch vụ</h3>
                  <p className="p-2">Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng, tân
                    tiến.</p>
                </div>
              </div>
            </div>

            {/* <!-- Title what we offer --> */}
            <div className="col-sm-12 text-center text-warning p-4 mt-4">
              <h2><b className="p-1 border-bottom">Menu combo Pizza 365</b></h2>
              <p><span className="p-2">Hãy chọn combo phù hợp với bạn</span></p>
            </div>

            {/* <!--  Content what we offer --> */}
            <div className="col-sm-12">
              <div class="row row-cols-1 row-cols-md-3 g-4">
                <div className="col">
                  <div className="card h-100">
                    <div className="card-header bg-warning text-center">
                      <h3>S ( small )</h3>
                    </div>
                    <div className="card-body text-center">
                      <ul className="list-group list-group-flush">
                        <li className="list-group-item">
                          Đường kính <b> 20 cm</b>
                        </li>
                        <li className="list-group-item">Sườn nướng <b>2</b></li>
                        <li className="list-group-item">Salad <b>200 gr</b></li>
                        <li className="list-group-item">Nước ngọt <b>2</b></li>
                        <li className="list-group-item">
                          <h1><b>150.000</b></h1>
                          <p>VNĐ</p>
                        </li>
                      </ul>
                    </div>
                    <div className="card-footer text-center">
                      <button className="btn btn-warning w-100" id="btn-small" data-is-selected-menu="N">
                        <b>Chọn</b>
                      </button>
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className="card h-100">
                    <div className="card-header bg-warning text-center">
                      <h3>M ( medium )</h3>
                    </div>
                    <div className="card-body text-center">
                      <ul className="list-group list-group-flush">
                        <li className="list-group-item">
                          Đường kính <b> 25 cm</b>
                        </li>
                        <li className="list-group-item">Sườn nướng <b>4</b></li>
                        <li className="list-group-item">Salad <b> 300 gr</b></li>
                        <li className="list-group-item">Nước ngọt <b>3</b></li>
                        <li className="list-group-item">
                          <h1><b>200.000</b></h1>
                          <p>VNĐ</p>
                        </li>
                      </ul>
                    </div>
                    <div className="card-footer text-center">
                      <button className="btn btn-warning w-100" id="btn-medium" data-is-selected-menu="N">
                        <b>Chọn</b>
                      </button>
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className="card h-100">
                    <div className="card-header bg-warning text-center">
                      <h3>L ( large )</h3>
                    </div>
                    <div className="card-body text-center">
                      <ul className="list-group list-group-flush">
                        <li className="list-group-item">
                          Đường kính <b> 30 cm</b>
                        </li>
                        <li className="list-group-item">Sườn nướng <b>8</b></li>
                        <li className="list-group-item">Salad <b> 500 gr</b></li>
                        <li className="list-group-item">Nước ngọt <b>4</b></li>
                        <li className="list-group-item">
                          <h1><b>250.000</b></h1>
                          <p>VNĐ</p>
                        </li>
                      </ul>
                    </div>
                    <div className="card-footer text-center">
                      <button className="btn btn-warning w-100" id="btn-large" data-is-selected-menu="N">
                        <b>Chọn</b>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* <!-- Title Who We Are --> */}
            <div className="col-sm-12 text-center text-warning p-4 mt-4">
              <h2><b className="p-2 border-bottom">Chọn loại pizza</b></h2>
            </div>

            {/* <!-- Content Who We Are --> */}
            <div className="col-sm-12">
              <div className="row row-cols-1 row-cols-md-3 g-4">
                <div className="col">
                  <div className="card">
                    <img src={require("../../assets/images/seafood.jpg")} className="card-img-top" alt="OCEAN MANIA" />
                    <div className="card-body">
                      <h6>OCEAN MANIA</h6>
                      <p>PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                      <p style={{ textTransform: "capitalize" }}>
                        xốt cà chua, phô mai mozzarella, tôm, mực, thanh cua, hành tây.
                      </p>

                      <button className="btn btn-warning w-100" id="btn-ocean" data-is-selected-pizza="N">
                        <b>Chọn</b>
                      </button>
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className="card">
                    <img src={require("../../assets/images/seafood.jpg")} className="card-img-top" alt="HAWAIIAN" />
                    <div className="card-body">
                      <h6> HAWAIIAN </h6>
                      <p>PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                      <p style={{ textTransform: "capitalize" }}>
                        xốt cà chua, phô mai mozzarella, thịt dăm bông, thơm.
                      </p>
                      <button className="btn btn-warning w-100" id="btn-hawai" data-is-selected-pizza="N">
                        <b>Chọn</b>
                      </button>
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className="card">
                    <img src={require("../../assets/images/seafood.jpg")} className="card-img-top" alt="CHICKEN" />
                    <div className="card-body">
                      <h6>CHESSY CHICKEN BACON</h6>
                      <p>PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                      <p style={{ textTransform: "capitalize" }}>
                        xốt phô mai, thịt gà, thịt heo muối, phô mai mozzarella, cà chua.
                      </p>
                      <button className="btn btn-warning w-100" id="btn-bacon" data-is-selected-pizza="N">
                        <b>Chọn</b>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* <!-- Title Drink --> */}
            <div className="col-sm-12 text-center text-warning p-4 mt-4">
              <h2><b className="p-2 border-bottom">Chọn đồ uống</b></h2>
            </div>
            {/* <!-- Content Drink --> */}
            <div className="col-sm-12">
              <select id="select-drink" className="form-select">
                <option value="0">-- Chọn đồ uống --</option>
                <option value="1"> Coca </option>
                <option value="2"> PepSi </option>
                <option value="3"> Fanta </option>
              </select>
            </div>

            {/* <!-- Title Contact Us --> */}
            <div className="col-sm-12 text-warning text-center p-4 mt-4">
              <h2><b className="p-2 border-bottom">Gửi đơn hàng</b></h2>
            </div>

            {/* <!-- Content Contact Us --> */}
            <div className="col-sm-12 p-2 ">
              <div className="row">
                <div className="col-sm-12">
                  <div className="mb-3">
                    <label className="form-label" for="inp-name">Họ và tên</label>
                    <input type="text" className="form-control" id="inp-name" placeholder="Họ và tên" />
                  </div>
                  <div className="mb-3">
                    <label className="form-label" for="email">Email</label>
                    <input type="text" className="form-control" id="inp-email" placeholder="Email" />
                  </div>
                  <div className="mb-3">
                    <label className="form-label" for="inp-phone">Điện thoại</label>
                    <input type="text" className="form-control" id="inp-phone" placeholder="Điện thoại" />
                  </div>
                  <div className="mb-3">
                    <label className="form-label" for="inp-address">Địa chỉ</label>
                    <input type="text" className="form-control" id="inp-address" placeholder="Địa chỉ" />
                  </div>
                  <div className="mb-3">
                    <label className="form-label" for="message">Lời nhắn</label>
                    <input type="text" className="form-control" id="inp-message" placeholder="Lời nhắn" />
                  </div>
                  <div className="mb-3">
                    <label className="form-label" for="inp-voucher">Mã giảm giá</label>
                    <input type="text" className="form-control" id="inp-voucher" placeholder="Mã giảm giá" />
                  </div>
                  <button type="button" className="btn btn-warning w-100" id="btn-send">
                    <b>Gửi</b>
                  </button>

                </div>
              </div>
            </div>
          </div>
        </div>

      </>
    )
  }
}

export default Content