import { Component } from "react";

class Header extends Component {
    render(){
        return (
            <>
            {/* <!-- Menu --> */}
      <div className="container-fluid">
        <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-warning mb-5">
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav nav-fill w-100">
              <li className="nav-item active">
                <a className="nav-link" href="/#">Trang chủ</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#">Combo</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#">Loại pizza</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#">Gửi đơn hàng</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
            </>
        )
    }
}

export default Header